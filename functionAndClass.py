# Funcion que suma 3 numeros

def sumaDeTress(a, b, c):
    return a + b + c


resultado = sumaDeTress(24, 4, 6)
print(resultado)

# Ejemplo de una clase llamada Car


class Car:
    def __init__(self, num_puertas, num_ruedas, car_color):
        self.num_puertas = num_puertas
        self.num_ruedas = num_ruedas
        self.car_color = car_color

    def ver_num_puetas(self):
        print("tu Carro Tiene ahora ", self.num_puertas, "Puertas")

    def agregar_puertas(self, puerta):
        if puerta > 0:
            self.num_puertas += puerta


my_car = Car(2, 4, "Negro")
my_car.agregar_puertas(2)
my_car.ver_num_puetas()
